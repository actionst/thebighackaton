<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/chart1', function (){
   return view('chart1');
})->name('chart1');

Route::get('/chart2', function (){
   return view('chart2');
})->name('chart2');

Route::get('/chart3', function (){
   return view('chart3');
})->name('chart3');

Route::get('/def', 'Controller@def')->name('def');

Route::post('/chart3', 'Controller@filter')->name('chart3');

Route::post('/search', 'Controller@search')->name('search');
