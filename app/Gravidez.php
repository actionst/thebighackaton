<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gravidez extends Model
{
    //
    protected $table="gravidez";
    protected $fillable=["nome", "usuario_id", "periodo"];
}
