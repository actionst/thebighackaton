<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escolaridade extends Model
{
    //
    protected $table="escolaridades";
    protected $fillable=["nome"];
}
