<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SituacaoAluno extends Model
{
    //
    protected $table="situacao_aluno";
    protected $fillable=["nome"];
}
