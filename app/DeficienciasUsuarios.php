<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeficienciasUsuarios extends Model
{
    //
    protected $table="deficiencias_usuarios";
    protected $fillable = ['usuario_id', 'deficiencia_id'];
}
