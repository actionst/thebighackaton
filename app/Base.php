<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    //
    protected $table="base";
    protected $fillable=[

        "ANO",
        "CD_SEXO",
        "CD_RACA_COR",
        "IDADE_ALUNO_ANO_CIVIL",
        "DEF_N_POSSUI",
        "CD_INEP_ALUNO",
        "LATITUDE",
        "LONGITUDE",
        'SITUACAO_MAT',


    ];
}
