<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrabalhoInfatilUsuarios extends Model
{
    //
    protected $table="trabalho_infantil_usuarios";
    protected $fillable = ['usuario_id', 'trabalho_infantil_id'];
}
