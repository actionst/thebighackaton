<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrabalhoInfatil extends Model
{
    //
    protected $table="trabalho_infantil";
    protected $fillable=["nome"];
}
