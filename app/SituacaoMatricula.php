<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SituacaoMatricula extends Model
{
    //
    protected $table="situacao_matricula";
    protected $fillable=["nome"];
}
