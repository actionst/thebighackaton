<?php

namespace App\Http\Controllers;

use App\base;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function dashboard(){
        $alunoDeficiencia = Base::where('DEF_N_POSSUI',0)->count();
        return $alunoDeficiencia;
    }

    public function def(){
        $cidades = Base::all();
       /* /*$geral = count($cidades);
        $dados = Base::all();
        $def = [];
        $raca = [];
        $sexo = [];
        $idade = [];
        $indices=[];
        foreach($dados as $index => $dado){
            $indices[$dado->MUNICIPIO]['DEF_N_POSSUI']
            if(!$indices[$dado->MUNICIPIO]['DEF_N_POSSUI'])
                $indices[$dado->MUNICIPIO]['DEF_N_POSSUI']=1;
            else
                $indices[$dado->MUNICIPIO]['DEF_N_POSSUI'];
            if(!$indices[$dado->MUNICIPIO]['CD_RACA_COR'])
                $indices[$dado->MUNICIPIO]['CD_RACA_COR']=1;
            else
                $indices[$dado->MUNICIPIO]['CD_RACA_COR']++;
            if(!$indices[$dado->MUNICIPIO]['CD_SEXO'])
                $indices[$dado->MUNICIPIO]['CD_SEXO']=1;
            else
                $indices[$dado->MUNICIPIO]['CD_SEXO']++;
            if(!$indices[$dado->MUNICIPIO]['IDADE_ALUNO_ANO_CIVIL'])
                $indices[$dado->MUNICIPIO]['IDADE_ALUNO_ANO_CIVIL']=1;
            else
                $indices[$dado->MUNICIPIO]['IDADE_ALUNO_ANO_CIVIL']++;

        }

        $indice = (((($dado->DEF_N_POSSUI) ? 1 : 0 )*2.5) + ((($dado->CD_RACA_COR) ? 1 : 0 )*2.5) + ((($dado->CD_SEXO) ? 1 : 0 )*2.5) + ((($dado->IDADE_ALUNO_ANO_CIVIL) ? 1 : 0 )*2.5))/4;


        $cidades = Cidade::where('estado_id',35)->get();*/

        return view('welcome', compact('cidades'));
    }

    public function search(Request $request){

        $geral = Base::count();
        $def = Base::where('DEF_N_POSSUI', 0)->count();
        $defGeral = Base::where('DEF_N_POSSUI', 1)->count();
        $def = ($request->def) ? Base::where('CD_RACA_COR', $request->def)->count() : 0;
        $defGeral = ($request->def) ? Base::where('CD_RACA_COR', '<>', $request->def)->count() : $geral;
        $raca = ($request->raca) ? Base::where('CD_RACA_COR', $request->raca)->count() : 0;
        $racaGeral = ($request->raca) ? Base::where('CD_RACA_COR', '<>', $request->raca)->count() : $geral;
        $sexo = ($request->sexo) ? Base::where('CD_SEXO', $request->sexo)->count() : 0;
        $sexoGeral = ($request->sexo) ? Base::where('CD_SEXO', '<>', $request->sexo)->count() : $geral;
        if($request->idade){
            $idade = ($request->idade==1) ? Base::where('IDADE_ALUNO_ANO_CIVIL', '>=', 6)->where('IDADE_ALUNO_ANO_CIVIL', '<=', 14)->count() : Base::where('IDADE_ALUNO_ANO_CIVIL', '>=', 15)->where('IDADE_ALUNO_ANO_CIVIL', '<=', 17)->count();
            $idadeGeral = ($request->idade==1) ? Base::where('IDADE_ALUNO_ANO_CIVIL', '<', 6)->where('IDADE_ALUNO_ANO_CIVIL', '>', 14)->count() : Base::where('IDADE_ALUNO_ANO_CIVIL', '<', 15)->where('IDADE_ALUNO_ANO_CIVIL', '>', 17)->count();
        }
        else{
            $idade = 0;
            $idadeGeral = $geral;
        }

        $indice = (($def*2.5) + ($raca*2.5) + ($sexo*2.5) + ($idade*2.5))/4;
        return view('chart3')
        ->with('geral', $geral)
        ->with('def', $def)
        ->with('defGeral', $defGeral)
        ->with('raca', $raca)
        ->with('racaGeral', $racaGeral)
        ->with('sexo', $sexo)
        ->with('sexoGeral', $sexoGeral)
        ->with('idadeGeral', $idadeGeral)
        ->with('idade', $idade)
        ->with('indice', $indice)
        ->with('dados', $request);
    }

}
