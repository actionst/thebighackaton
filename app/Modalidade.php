<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modalidade extends Model
{
    //
    protected $table="modalidades";
    protected $fillable=["nome", "descricao"];
}
