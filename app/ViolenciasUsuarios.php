<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViolenciasUsuarios extends Model
{
    //
    protected $table="violencias_usuarios";
    protected $fillable = ['usuario_id', 'violencia_id', 'descricao'];
}
