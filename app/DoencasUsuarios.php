<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoencasUsuarios extends Model
{
    //
    protected $table="doencas_usuarios";
    protected $fillable = ['usuario_id', 'doenca_id'];
}
