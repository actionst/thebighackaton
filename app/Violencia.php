<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Violencia extends Model
{
    //
    protected $table="violencia";
    protected $fillable=["nome", "usuario_id", "data_ocorrencia"];
}
