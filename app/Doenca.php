<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doenca extends Model
{
    //
    protected $table="doencas";
    protected $fillable=["nome"];
}
