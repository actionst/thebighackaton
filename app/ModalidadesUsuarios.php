<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModalidadesUsuarios extends Model
{
    //
    protected $table="modalidades_usuarios";
    protected $fillable = ['usuario_id', 'modalidade_id'];
}
