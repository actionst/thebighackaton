<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovimentoSituacao extends Model
{
    //
    protected $table="movimento_situacao";
    protected $fillable=["usuario_id", "situacao_aluno_id"];
}
