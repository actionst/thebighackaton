<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RacasUsuarios extends Model
{
    //
    protected $table="racas_usuarios";
    protected $fillable = ['usuario_id', 'raca_id'];
}
