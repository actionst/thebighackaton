<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    use Notifiable;

    protected $table = 'usuarios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'email', 'id_turma', 'id_turno', 'id_serie', 'id_modalidade', 'id_raca', 'id_escolaridade', 'id_cidade_nascimento', 'id_estado_nascimento', 'id_situacao_matricula', 'cod_aluno_sme', 'nascimento', 'sexo', 'possui_deficiencia', 'renda_familiar', 'id_escolaridade_pai', 'id_escolaridade_mae', 'pais_casados', 'responsavel_legal'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
