<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deficiencia extends Model
{
    //
    protected $table="deficiencias";
    protected $fillable=["nome"];
}
