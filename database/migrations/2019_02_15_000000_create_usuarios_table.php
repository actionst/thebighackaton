<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            //$table->integer('turma_id')->unsigned();
            $table->integer('turno_id')->unsigned();
            //$table->integer('serie_id')->unsigned();
            $table->integer('modalidade_id')->unsigned();
            $table->integer('raca_id')->unsigned();
            //$table->integer('cidade_nascimento_id')->unsigned();
            //$table->integer('estado_nascimento_id')->unsigned();
            $table->string('cidade_nascimento_nome');
            $table->string('estado_nascimento_sigla');
            $table->integer('situacao_matricula_id')->unsigned();
            $table->double('cod_aluno_sme');
            $table->integer('nascimento');
            $table->enum('sexo', ['Masculino', 'Feminino']);
            $table->integer('ano');
            $table->integer('cod_unidade');
            $table->string('distrito');
            $table->string('escola');
            $table->boolean('nee_alt_hab');
            $table->boolean('def_autismo');
            $table->boolean('def_surdez_leve');
            $table->boolean('def_surdez_sev');
            $table->boolean('def_intelect');
            $table->boolean('def_multipla');
            $table->boolean('def_cegueira');
            $table->boolean('def_baixa_visao');
            $table->boolean('def_surdo_ceg');
            $table->boolean('def_transt_des_inf');
            $table->boolean('def_sindr_asper');
            $table->boolean('def_sindr_rett');
            $table->boolean('def_fis_n_cadeir');
            $table->boolean('def_fis_cadeir');
            $table->boolean('def_n_possui');

            $table->rememberToken();

            //$table->foreign('turma_id')->references('id')->on('turmas')->onDelete('cascade');
            $table->foreign('turno_id')->references('id')->on('turnos')->onDelete('cascade');
            //$table->foreign('serie_id')->references('id')->on('series')->onDelete('cascade');
            $table->foreign('modalidade_id')->references('id')->on('modalidades')->onDelete('cascade');
            $table->foreign('raca_id')->references('id')->on('racas')->onDelete('cascade');
            //$table->foreign('cidade_nascimento_id')->references('id')->on('cidades')->onDelete('cascade');
            //$table->foreign('estado_nascimento_id')->references('id')->on('estados')->onDelete('cascade');
            $table->foreign('situacao_matricula_id')->references('id')->on('situacao_matricula')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}

//turno_id,modalidade_id,raca_id,cidade_nascimento_nome,estado_nascimento_sigla,situacao_matricula_id,cod_aluno_sme,nascimento,sexo,ano,cod_unidade,distrito,escola,nee_alt_hab,def_autismo,def_surdez_leve,def_surdez_sev,def_intelect,def_multipla,def_cegueira,def_baixa_visao,def_surdo_ceg,def_transt_des_inf,def_sindr_asper,def_sindr_rett,def_fis_n_cadeir,def_fis_cadeir,def_n_possui
