<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscolasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escolas', function (Blueprint $table) {
            $table->increments('cod_unidade');
            $table->string('distrito');
            $table->string('escola');
            //$table->integer('id_cidade')->unsigned();
            //$table->integer('id_estado')->unsigned();
            $table->string('nome_cidade')->nullable();
            $table->string('sigla_estado')->nullable();
            $table->timestamps();
            
            //$table->foreign('id_cidade')->references('id')->on('cidades')->onDelete('cascade');
            //$table->foreign('id_estado')->references('id')->on('estados')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('escolas');
    }
}
