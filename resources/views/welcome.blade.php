@extends('template')

@section('head')
    <script type='text/javascript'>
        google.charts.load('current', {
            'packages': ['geochart'],
            'mapsApiKey': 'AIzaSyDZKJxTaiFg8YE5A2qlmhgXtH_p2xdzv2s',
        });
        google.charts.setOnLoadCallback(drawMarkersMap);

        function drawMarkersMap() {
            var data = google.visualization.arrayToDataTable([
                ['City',   'Population', 'Area'],
                ['Rome',      2761477,    1285.31],
                ['Milan',     1324110,    181.76],
                ['Naples',    959574,     117.27],
                ['Turin',     907563,     130.17],
                ['Palermo',   655875,     158.9],
                ['Genoa',     607906,     243.60],
                ['Bologna',   380181,     140.7],
                ['Florence',  371282,     102.41],
                ['Fiumicino', 67370,      213.44],
                ['Anzio',     52192,      43.43],
                ['Ciampino',  38262,      11]
            ]);

            var options = {
                region: 'IT',
                displayMode: 'markers',
                colorAxis: {colors: ['green', 'blue']}
            };

            var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        };
    </script>

    <script type="text/javascript">
        jQuery(function ($) {

            var chart3 = $('#geoChart').chartinator({
                tableSel: '.geoChart',

                columns: [{role: 'tooltip', type: 'string'}],

                colIndexes: [2],

                rows: [
                    ['China - 2015'],
                    ['Colombia - 2015'],
                    ['France - 2015'],
                    ['Italy - 2015'],
                    ['Japan - 2015'],
                    ['Kazakhstan - 2015'],
                    ['Mexico - 2015'],
                    ['Poland - 2015'],
                    ['Russia - 2015'],
                    ['Spain - 2015'],
                    ['Tanzania - 2015'],
                    ['Turkey - 2015']],

                ignoreCol: [2],

                chartType: 'GeoChart',

                chartAspectRatio: 1.5,

                chartZoom: 1.75,

                chartOffset: [-12,0],

                chartOptions: {

                    width: null,

                    backgroundColor: '#fff',

                    datalessRegionColor: '#F5F5F5',

                    region: 'world',

                    resolution: 'countries',

                    legend: 'none',

                    colorAxis: {

                        colors: ['#679CCA', '#337AB7']
                    },
                    tooltip: {

                        trigger: 'focus',

                        isHtml: true
                    }
                }


            });
        });
    </script>
    <!--geo chart-->

    <!--skycons-icons-->
    <script src={{asset('public/assets/js/skycons.js')}}></script>
    <!--//skycons-icons-->
@endsection

@section('content')
<div class="page-container">
    <div class="left-content">
        <div class="mother-grid-inner">
            <div class="inner-block" >
                <!--market updates updates--><div class="market-updates" style="margin-top: -100px">
                    <div class="col-md-4 market-update-gd">
                        <div class="market-update-block clr-block-1">
                            <div class="col-md-8 market-update-left">
                                <a href="{{route('def')}}">
                                    <h3>3</h3>
                                    <h4>Perfis critícos</h4>

                                </a>
                            </div>
                            <div class="col-md-4 market-update-right">
                                <i class="fa fa-file-text-o"></i>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="col-md-4 market-update-gd">
                        <div class="market-update-block clr-block-2">
                            <div class="col-md-8 market-update-left">
                                <a href="{{route('chart2')}}">
                                    <h3>São Paulo</h3>
                                    <h4>Evasão por cidade</h4>
                                </a>
                            </div>
                            <div class="col-md-4 market-update-right">
                                <i class="fa fa-file-text-o"> </i>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="col-md-4 market-update-gd">

                        <div class="market-update-block clr-block-3">
                            <div class="col-md-8 market-update-left">
                                <a href="{{route('chart3')}}">
                                    <h3>Filtros</h3>
                                    <h4>Perspectiva de perfis</h4>
                                </a>
                            </div>
                            <div class="col-md-4 market-update-right">
                                <i class="fa fa-eye"> </i>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div><br/>
                <div class="clearfix"> </div>
                <!--market updates end here-->
                <!--mainpage chit-chating-->
              {{--  <div class="chit-chat-layer1">
                    <div class="col-md-6 chit-chat-layer1-left">
                        <div class="work-progres">
                            <div class="chit-chat-heading">
                                Recent Followers
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Project</th>
                                        <th>Manager</th>

                                        <th>Status</th>
                                        <th>Progress</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Face book</td>
                                        <td>Malorum</td>

                                        <td><span class="label label-danger">in progress</span></td>
                                        <td><span class="badge badge-info">50%</span></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Twitter</td>
                                        <td>Evan</td>

                                        <td><span class="label label-success">completed</span></td>
                                        <td><span class="badge badge-success">100%</span></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Google</td>
                                        <td>John</td>

                                        <td><span class="label label-warning">in progress</span></td>
                                        <td><span class="badge badge-warning">75%</span></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>LinkedIn</td>
                                        <td>Danial</td>

                                        <td><span class="label label-info">in progress</span></td>
                                        <td><span class="badge badge-info">65%</span></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Tumblr</td>
                                        <td>David</td>

                                        <td><span class="label label-warning">in progress</span></td>
                                        <td><span class="badge badge-danger">95%</span></td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Tesla</td>
                                        <td>Mickey</td>

                                        <td><span class="label label-info">in progress</span></td>
                                        <td><span class="badge badge-success">95%</span></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 chit-chat-layer1-rit">
                        <div class="geo-chart">
                            <section id="charts1" class="charts">
                                <div class="wrapper-flex">

                                    <table id="myTable" class="geoChart tableChart data-table col-table" style="display:none;">
                                        <caption>Student Nationalities Table</caption>
                                        <tr>
                                            <th scope="col" data-type="string">Country</th>
                                            <th scope="col" data-type="number">Number of Students</th>
                                            <th scope="col" data-role="annotation">Annotation</th>
                                        </tr>
                                        <tr>
                                            <td>China</td>
                                            <td align="right">20</td>
                                            <td align="right">20</td>
                                        </tr>
                                        <tr>
                                            <td>Colombia</td>
                                            <td align="right">5</td>
                                            <td align="right">5</td>
                                        </tr>
                                        <tr>
                                            <td>France</td>
                                            <td align="right">3</td>
                                            <td align="right">3</td>
                                        </tr>
                                        <tr>
                                            <td>Italy</td>
                                            <td align="right">1</td>
                                            <td align="right">1</td>
                                        </tr>
                                        <tr>
                                            <td>Japan</td>
                                            <td align="right">18</td>
                                            <td align="right">18</td>
                                        </tr>
                                        <tr>
                                            <td>Kazakhstan</td>
                                            <td align="right">1</td>
                                            <td align="right">1</td>
                                        </tr>
                                        <tr>
                                            <td>Mexico</td>
                                            <td align="right">1</td>
                                            <td align="right">1</td>
                                        </tr>
                                        <tr>
                                            <td>Poland</td>
                                            <td align="right">1</td>
                                            <td align="right">1</td>
                                        </tr>
                                        <tr>
                                            <td>Russia</td>
                                            <td align="right">11</td>
                                            <td align="right">11</td>
                                        </tr>
                                        <tr>
                                            <td>Spain</td>
                                            <td align="right">2</td>
                                            <td align="right">2</td>
                                        </tr>
                                        <tr>
                                            <td>Tanzania</td>
                                            <td align="right">1</td>
                                            <td align="right">1</td>
                                        </tr>
                                        <tr>
                                            <td>Turkey</td>
                                            <td align="right">2</td>
                                            <td align="right">2</td>
                                        </tr>

                                    </table>

                                    <div class="col geo_main">
                                        <h3 id="geoChartTitle">World Market</h3>
                                        <div id="geoChart" class="chart"> </div>
                                    </div>


                                </div><!-- .wrapper-flex -->
                            </section>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>--}}
                <!--main page chit chating end here-->
                <!--main page chart start here-->
               {{-- <div class="main-page-charts">
                    <div class="main-page-chart-layer1">
                        <div class="col-md-6 chart-layer1-left">
                            <div class="glocy-chart">
                                <div class="span-2c">
                                    <h3 class="tlt">Sales Analytics</h3>
                                    <canvas id="bar" height="300" width="400" style="width: 400px; height: 300px;"></canvas>
                                    <script>
                                        var barChartData = {
                                            labels : ["Jan","Feb","Mar","Apr","May","Jun","jul"],
                                            datasets : [
                                                {
                                                    fillColor : "#FC8213",
                                                    data : [65,59,90,81,56,55,40]
                                                },
                                                {
                                                    fillColor : "#337AB7",
                                                    data : [28,48,40,19,96,27,100]
                                                }
                                            ]

                                        };
                                        new Chart(document.getElementById("bar").getContext("2d")).Bar(barChartData);

                                    </script>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 chart-layer1-right">
                            <div class="user-marorm">
                                <div class="malorum-top">
                                </div>
                                <div class="malorm-bottom">
                                    <span class="malorum-pro"> </span>
                                    <h4>unde omnis iste</h4>
                                    <h2>Melorum</h2>
                                    <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising.</p>
                                    <ul class="malorum-icons">
                                        <li><a href="#"><i class="fa fa-facebook"> </i>
                                                <div class="tooltip"><span>Facebook</span></div>
                                            </a></li>
                                        <li><a href="#"><i class="fa fa-twitter"> </i>
                                                <div class="tooltip"><span>Twitter</span></div>
                                            </a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"> </i>
                                                <div class="tooltip"><span>Google</span></div>
                                            </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>--}}
                <!--main page chart layer2-->
                {{--<div class="chart-layer-2">

                    <div class="col-md-6 chart-layer2-right">
                        <div class="prograc-blocks">
                            <!--Progress bars-->
                            <div class="home-progres-main">
                                <h3>Total Sales</h3>
                            </div>
                            <div class='bar_group'>
                                <div class='bar_group__bar thin' label='Rating' show_values='true' tooltip='true' value='343'></div>
                                <div class='bar_group__bar thin' label='Quality' show_values='true' tooltip='true' value='235'></div>
                                <div class='bar_group__bar thin' label='Amount' show_values='true' tooltip='true' value='550'></div>
                                <div class='bar_group__bar thin' label='Farming' show_values='true' tooltip='true' value='456'></div>
                            </div>
                            <script src="js/bars.js"></script>

                            <!--//Progress bars-->
                        </div>
                    </div>
                    <div class="col-md-6 chart-layer2-left">
                        <div class="content-main revenue">
                            <h3>Total Revenue</h3>
                            <canvas id="radar" height="300" width="300" style="width: 300px; height: 300px;"></canvas>
                            <script>
                                var radarChartData = {
                                    labels : ["","","","","","",""],
                                    datasets : [
                                        {
                                            fillColor : "rgba(104, 174, 0, 0.83)",
                                            strokeColor : "#68ae00",
                                            pointColor : "#68ae00",
                                            pointStrokeColor : "#fff",
                                            data : [65,59,90,81,56,55,40]
                                        },
                                        {
                                            fillColor : "rgba(236, 133, 38, 0.82)",
                                            strokeColor : "#ec8526",
                                            pointColor : "#ec8526",
                                            pointStrokeColor : "#fff",
                                            data : [28,48,40,19,96,27,100]
                                        }
                                    ]

                                };
                                new Chart(document.getElementById("radar").getContext("2d")).Radar(radarChartData);
                            </script>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>--}}

                <!--climate start here-->
                {{--<div class="climate">
                    <div class="col-md-4 climate-grids">
                        <div class="climate-grid1">
                            <div class="climate-gd1-top">
                                <div class="col-md-6 climate-gd1top-left">
                                    <h4>Aprill 6-wed</h4>
                                    <h3>12:30<span class="timein-pms">PM</span></h3>
                                    <p>Humidity:</p>
                                    <p>Sunset:</p>
                                    <p>Sunrise:</p>
                                </div>
                                <div class="col-md-6 climate-gd1top-right">
					  <span class="clime-icon">
					  	<figure class="icons">
								<canvas id="partly-cloudy-day" width="64" height="64">
								</canvas>
							</figure>
						<script>
							 var icons = new Skycons({"color": "#fff"}),
                                 list  = [
                                     "clear-night", "partly-cloudy-day",
                                     "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                                     "fog"
                                 ],
                                 i;

                             for(i = list.length; i--; )
                                 icons.set(list[i], list[i]);

                             icons.play();
						</script>
				   </span>
                                    <p>88%</p>
                                    <p>5:40PM</p>
                                    <p>6:30AM</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="climate-gd1-bottom">
                                <div class="col-md-4 cloudy1">
                                    <h4>Hongkong</h4>
                                    <figure class="icons">
                                        <canvas id="sleet" width="58" height="58">
                                        </canvas>
                                    </figure>
                                    <script>
                                        var icons = new Skycons({"color": "#fff"}),
                                            list  = [
                                                "clear-night", "clear-day",
                                                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                                                "fog"
                                            ],
                                            i;

                                        for(i = list.length; i--; )
                                            icons.set(list[i], list[i]);

                                        icons.play();
                                    </script>
                                    <h3>10c</h3>
                                </div>
                                <div class="col-md-4 cloudy1">
                                    <h4>UK</h4>
                                    <figure class="icons">
                                        <canvas id="cloudy" width="58" height="58"></canvas>
                                    </figure>
                                    <script>
                                        var icons = new Skycons({"color": "#fff"}),
                                            list  = [
                                                "clear-night", "cloudy",
                                                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                                                "fog"
                                            ],
                                            i;

                                        for(i = list.length; i--; )
                                            icons.set(list[i], list[i]);

                                        icons.play();
                                    </script>
                                    <h3>6c</h3>
                                </div>
                                <div class="col-md-4 cloudy1">
                                    <h4>USA</h4>
                                    <figure class="icons">
                                        <canvas id="snow" width="58" height="58">
                                        </canvas>
                                    </figure>
                                    <script>
                                        var icons = new Skycons({"color": "#fff"}),
                                            list  = [
                                                "clear-night", "clear-day",
                                                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                                                "fog"
                                            ],
                                            i;

                                        for(i = list.length; i--; )
                                            icons.set(list[i], list[i]);

                                        icons.play();
                                    </script>
                                    <h3>10c</h3>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 climate-grids">
                        <div class="climate-grid2">
                            <span class="shoppy-rate"><h4>$180</h4></span>
                            <ul>
                                <li> <i class="fa fa-credit-card"> </i> </li>
                                <li> <i class="fa fa-usd"> </i> </li>
                            </ul>
                        </div>
                        <div class="shoppy">
                            <h3>Those Who Hate Shopping?</h3>
                        </div>
                    </div>
                    <div class="col-md-4 climate-grids">
                        <div class="climate-grid3">
                            <div class="popular-brand">
                                <div class="col-md-6 popular-bran-left">
                                    <h3>Popular</h3>
                                    <h4>Brand of this month</h4>
                                    <p> Duis aute irure  in reprehenderit.</p>
                                </div>
                                <div class="col-md-6 popular-bran-right">
                                    <h3>Polo</h3>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="popular-follow">
                                <div class="col-md-6 popular-follo-left">
                                    <p>Lorem ipsum dolor sit amet, adipiscing elit.</p>
                                </div>
                                <div class="col-md-6 popular-follo-right">
                                    <h4>Follower</h4>
                                    <h5>2892</h5>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>--}}
                <!--climate end here-->
                <div class="col-md-12 chit-chat-layer1-rit">
                    <h2>Classificação com dados gerais</h2>
                    <div class="btn-group" style="margin-bottom: 20px">
                        <button type="button" id="alto" class="btn btn-danger">Risco alto</button>
                        <button type="button" id="medio" class="btn btn-info">Risco médio</button>
                        <button type="button" id="baixo" class="btn btn-success">Risco baixo</button>
                    </div>
                    <div class="geo-chart">
                        <section id="charts1" class="charts">
                            <div class="wrapper-flex">
                                <div class="col geo_main">
                                    <h3 id="geoChartTitle">Evasão Escolar em São Paulo</h3>
                                    <div id="mapid" style="height: 400px"></div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <input type="hidden" value="{{$cidades ?? ''}}" id="cidades"/>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>


@endsection
<!--slide bar menu end here-->



@section('scripts')

    <script>
        var toggle = true;

        $(".sidebar-icon").click(function() {
            if (toggle)
            {
                $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
                $("#menu span").css({"position":"absolute"});
            }
            else
            {
                $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
                setTimeout(function() {
                    $("#menu span").css({"position":"relative"});
                }, 400);
            }
            toggle = !toggle;
        });
    </script>

    <script>
        $(document).ready(function() {
            var navoffeset=$(".header-main").offset().top;
            $(window).scroll(function(){
                var scrollpos=$(window).scrollTop();
                if(scrollpos >=navoffeset){
                    $(".header-main").addClass("fixed");
                }else{
                    $(".header-main").removeClass("fixed");
                }
            });

        });
    </script>
    <script src={{asset('public/assets/js/bars.js')}}></script>
    <script>
        var radarChartData = {
            labels : ["","","","","","",""],
            datasets : [
                {
                    fillColor : "rgba(104, 174, 0, 0.83)",
                    strokeColor : "#68ae00",
                    pointColor : "#68ae00",
                    pointStrokeColor : "#fff",
                    data : [65,59,90,81,56,55,40]
                },
                {
                    fillColor : "rgba(236, 133, 38, 0.82)",
                    strokeColor : "#ec8526",
                    pointColor : "#ec8526",
                    pointStrokeColor : "#fff",
                    data : [28,48,40,19,96,27,100]
                }
            ]

        };
        new Chart(document.getElementById("radar").getContext("2d")).Radar(radarChartData);
    </script>
    <script>
        $(document).ready(function() {
            var navoffeset=$(".header-main").offset().top;
            $(window).scroll(function(){
                var scrollpos=$(window).scrollTop();
                if(scrollpos >=navoffeset){
                    $(".header-main").addClass("fixed");
                }else{
                    $(".header-main").removeClass("fixed");
                }
            });

        });
    </script>
    <script>
        var toggle = true;

        $(".sidebar-icon").click(function() {
            if (toggle)
            {
                $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
                $("#menu span").css({"position":"absolute"});
            }
            else
            {
                $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
                setTimeout(function() {
                    $("#menu span").css({"position":"relative"});
                }, 400);
            }
            toggle = !toggle;
        });
    </script>
    <!--scrolling js-->
    <script src={{asset('public/assets/js/jquery.nicescroll.js')}}></script>
    <script src={{asset('public/assets/js/scripts.js')}}></script>
    <!--//scrolling js-->
    <script src={{asset('public/assets/js/bootstrap.js')}}> </script>
    <script>
        /*var map = L.map('mapid').setView([-23.5489, -46.6388], 7);*/

        let color = 'blue';


        let cidade = "São Paulo";
        let id = 1;
        let lat = -23.5489;
        let log = -46.6388;

        let cidades = $('#cidades').val();

        if(cidades != ''){
            cidades = JSON.parse(cidades);
        }

        console.log(cidades);

        let map = L.map('mapid',{
            center: [-23.5489, -46.6388], zoom: 10
        });
        let myDataPoint1 = '';


        cidades.forEach(function(data, item) {
            let indice = data.CD_ALUNO_SME


            if (indice > 4458000){
                color = "red"
            }if(indice >4440000 && indice < 4457999){
                color = "blue"
            }
            if(indice < 4439999){
                color = "green"
            }
            console.log(data.MUNICIPIO);

            let lat = data.LATITUDE.split('.')
            let latAux = lat[0]
            latAux = latAux.concat(`.${lat[1].concat(lat[2])}`)
            lat=latAux

            let lon = data.LONGITUDE.split('.')
            latAux = lon[0]
            latAux = latAux.concat(`.${lon[1].concat(lon[2])}`)
            lon=latAux

            myDataPoint1 = L.circle([lat, lon],{color: color, opacity:5, radius: 400}).addTo(map);
            myDataPoint1.bindPopup("<h3>"+ data.MUNICIPIO +"</h3>Índice<p><br>"+indice/1000000+"</p>");

        });

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: '© OpenStreetMap' }).addTo(map);

        var mapboxAccessToken = 'pk.eyJ1IjoiamJiYmF0aXN0YSIsImEiOiJjanM3Mm03aTkwd3FzNDN0Zm1scWJlbXZrIn0.P76KBL6yMKxyC_LmE7K83g';



        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + mapboxAccessToken, {
            id: 'mapbox.light',
            attribution: ''
        }).addTo(map);



        L.geoJson(statesData).addTo(map);

    </script>
@endsection
