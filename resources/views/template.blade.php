<!DOCTYPE HTML>
<html>
<head>
    <title>EducAção</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="icon" href="{{asset('public/img/icon.png')}}" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" />

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <link href={{asset('public/assets/css/bootstrap.css')}} rel="stylesheet" type="text/css" media="all">
    <!-- Custom Theme files -->
    <link href={{asset('public/assets/css/style.css')}} rel="stylesheet" type="text/css" media="all"/>
    <!--js-->
    <script src={{asset('public/assets/js/jquery-2.1.1.min.js')}}></script>
    <!--icons-css-->
    <link href={{asset('public/assets/css/font-awesome.css')}} rel="stylesheet">
    <!--Google Fonts-->
    <link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
    <!--static chart-->
    <script src={{asset('public/assets/js/Chart.min.js')}}></script>
    <!--//charts-->
    <!-- geo chart -->
    <script src={{asset('public/assets/cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js')}} type="text/javascript"></script>
    <script>window.modernizr || document.write('<script src={{asset('public/assets/lib/modernizr/modernizr-custom.js')}}><\/script>')</script>
    <!--<script src="lib/html5shiv/html5shiv.js"></script>-->
    <!-- Chartinator  -->
    <script src={{asset('public/assets/js/chartinator.js')}} ></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>


    <!--estilos leaft-->
    <!-- External Stylesheets -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />

    <!-- Add the Leaflet JavaScript library -->
    <script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>

    @yield('head')

</head>


<body>
<div class="header-main" style="width: 100%">
    <div class="logo-name">
        <a href="{{route('def')}}">
            <img id="logo" src="{{asset('public/img/educacao.png')}}" alt="Logo"  class="img-thumbnail" style="height: 60px"/>
        </a>
    </div>
</div>


{{--
<div class="sidebar-menu">



        <ul id="menu" >
            <li id="menu-home" ><a href="{{route('dashboard')}}"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>

            <li><a href="{{route('chart1')}}"><i class="fa fa-area-chart"></i><span>Mapa 1</span></a></li>

            <li><a href="{{route('chart2')}}"><i class="fa fa-line-chart"></i><span>Mapa 2</span></a></li>

            <li><a href="{{route('chart3')}}"><i class="fa fa-pie-chart"></i><span>Mapa 3</span></a></li>

        </ul>
</div>--}}
<div style="margin-top: 100px">
    @yield('content')
</div>


@yield('scripts')

<div class="copyrights">
    <p>© 2019 EducAção. Todos os Direitos Reservados </p>
</div>

</body>
</html>
